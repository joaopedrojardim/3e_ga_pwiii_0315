import { HomeModule } from './components/home/home.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { TrailComponent } from './components/trail/trail.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroUsuariosComponent } from './components/cadastro-usuarios/cadastro-usuarios.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StreetComponent,
    CustomComponent,
    TrailComponent,
    SportComponent,
    TouringComponent,
    LoginComponent,
    CadastroUsuariosComponent,
    CadastroClientesComponent,
    CadastroMotosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
