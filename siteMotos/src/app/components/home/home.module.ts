import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { SlidesComponent } from './slides/slides.component';
import { CardsComponent } from './cards/cards.component';
import { DestaquesComponent } from './destaques/destaques.component';
import { FooterComponent } from './footer/footer.component';
import { Cards2Component } from './cards2/cards2.component';



@NgModule({
  declarations: [
    NavbarComponent,
    SlidesComponent,
    CardsComponent,
    DestaquesComponent,
    FooterComponent,
    Cards2Component
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NavbarComponent,
    SlidesComponent,
    DestaquesComponent,
    FooterComponent,
    CardsComponent,
    Cards2Component
  ]
})
export class HomeModule { }
