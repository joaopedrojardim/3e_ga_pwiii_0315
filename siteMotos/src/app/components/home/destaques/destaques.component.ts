import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-destaques',
  templateUrl: './destaques.component.html',
  styleUrls: ['./destaques.component.css']
})
export class DestaquesComponent implements OnInit {

  MotosDestaque = [
    {
      indice : 0,
      imagem : "http://www.nordestemotos.com.br/images/CG_160_Titan.png",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 1,
      imagem : "https://www.honda.com.br/motos/sites/hda/files/styles/product_860x550/public/2022-03/PCX-prata.webp?itok=qSeAEWZ6",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 0,
      imagem : "https://www.honda.com.br/motos/sites/hda/files/styles/product_860x550/public/2022-03/CG-160-cargo-branca.webp?itok=nOvt6W7N",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 0,
      imagem : "http://www.nordestemotos.com.br/images/CG_160_Titan.png",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 1,
      imagem : "https://www.honda.com.br/motos/sites/hda/files/styles/product_860x550/public/2022-03/PCX-prata.webp?itok=qSeAEWZ6",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 0,
      imagem : "https://www.honda.com.br/motos/sites/hda/files/styles/product_860x550/public/2022-03/CG-160-cargo-branca.webp?itok=nOvt6W7N",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
    {
      indice : 0,
      imagem : "http://www.nordestemotos.com.br/images/CG_160_Titan.png",
      modelo : "Modelo da moto",
      marca :  "Marca",
      descricao : "Motociclo ou motocicleta, é um veículo motorizado de duas rodas e tracção traseira e salvo raras excepções, capaz de desenvolver velocidade de cruzeiro com segurança e conforto.",
      preco: 60.34
    },
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
